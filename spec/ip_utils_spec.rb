require 'rspec'
require 'rspec/expectations'
require_relative '../lib/ip_utils'

describe 'IP Utilities' do
  describe '.private_ip?' do
    it 'returns true when the first octet is 192.0.0.1' do
      expect(private_ip?('192.0.0.1')).to be true
    end
    it 'returns true when the first octet is 172.0.0.1' do
      expect(private_ip?('172.0.0.1')).to be true
    end
    it 'returns true when the first octet is 10.0.0.1' do
      expect(private_ip?('10.0.0.1')).to be true
    end

    it 'returns false when the first octet is 155' do
      expect(private_ip?('155.0.0.1')).to be false
    end

    it 'raises an exception when there are more than 4 octets' do
      expect { private_ip? '10.1.2.3.4' }.to raise_error InvalidIPAddressError
    end

    it 'raises an exception when there are less than 4 octets' do
      expect { private_ip? '10.1.2' }.to raise_error InvalidIPAddressError
    end

    it 'raises an exception when the first octet is greater than 255' do
      expect { private_ip? '256.1.2.3' }.to raise_error InvalidIPAddressError
    end

    it 'raises an exception when the second octet is greater than 255' do
      expect { private_ip? '10.1256.2.3' }.to raise_error InvalidIPAddressError
    end

    it 'raises an exception when the third octet is greater than 255' do
      expect { private_ip? '10.1.256.3' }.to raise_error InvalidIPAddressError
    end

    it 'raises an exception when the fourth octet is greater than 255' do
      expect { private_ip? '10.1.2.256' }.to raise_error InvalidIPAddressError
    end
  end
end
