# Description
Script that identifies if a provided IP address is public or private.
Returns true when the IP address is private, false when it is public, 
and an error for anything else

# Building the image
Install docker on the machine you will be running the command from.
You can follow instructions for docker CE at "https://docs.docker.com"

From the root directory, run the command `docker build -t iputils .`

# Running the application
Run the following command, providing the IP address as an input.
In the following example, replace '2.2.2.2' with an IP address of your choosing.

`docker run iputils ruby entry.rb 2.2.2.2`

To run the tests within the docker container;
`docker run iputils ruby rspec`

# Additional notes
This is a prototype, and does not represent a full featured application. 
Additional tests, and refactoring are recommended.

- When a non-numeric is entered, it will be replaced with 0