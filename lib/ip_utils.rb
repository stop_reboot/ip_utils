#!/usr/bin/env ruby

# This script can be used to find out if an IP address is public or private

# PUBLIC IP ADDRESS RANGE (RFC 1918): https://tools.ietf.org/html/rfc1918
# 10.0.0.0        -   10.255.255.255  (10/8 prefix)
# 172.16.0.0      -   172.31.255.255  (172.16/12 prefix)
# 192.168.0.0     -   192.168.255.255 (192.168/16 prefix)

PRIVATE_IP_ADDRESS_PREFIXES = [10, 172, 192].freeze

# Error used to report invalid IP address values
class InvalidIPAddressError < StandardError
  def initialize(msg = 'Not a valid IPv4 address.')
    super
  end
end

# Returns True if the provided IP address is public, false if it's not.
def private_ip?(ip_address)
  valid_ip? ip_address

  octets = ip_address.split('.')

  PRIVATE_IP_ADDRESS_PREFIXES.include? octets.first.to_i
end

# Check the validity of the IP address. Not an in-depth test.
def valid_ip?(ip_address)
  # Use a built in library for this instead of custom code
  octets = ip_address.split '.'

  raise InvalidIPAddressError unless octets.count == 4

  octets.each do |octet|
    raise InvalidIPAddressError unless octet.to_i.between?(0, 255)
  end

  true
end
