require_relative 'lib/ip_utils'

arg1 = ARGV[0]
puts private_ip? arg1


# Use the following to run this from a local command line
# begin
# puts 'Please enter an IP address'
# puts private_ip? gets.chomp.to_s
# rescue => error
#  puts error.message
# end
