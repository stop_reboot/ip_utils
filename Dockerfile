FROM ruby:2.2-onbuild
RUN mkdir /app
WORKDIR /app
ADD . /app
RUN bundle install
CMD [" bundle exec rake test /app/iputils"]